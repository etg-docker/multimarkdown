#!/bin/bash

source ../settings.sh

echo "Building hello world document."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/multimarkdown" \
	$IMAGENAME \
		"testdocs/hello_world.md"
